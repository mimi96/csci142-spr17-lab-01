package lock;

public class KeyLock implements Lock {

  // test is 6
  private int myKey;

  private boolean myIsLocked;

  private boolean myIsInserted;

  /**
   * make a lock for this key 
   * @param keyValue is the actual key
   */
  public KeyLock(int keyValue) 
  {
	  myKey = keyValue;
  }

  /**
   * 
   * @param keyValue is inserted into lock
   */
  public void insert(int keyValue) 
  {
	   if (myKey == keyValue) 
	   {
		   //they match
		   myIsInserted = true;
	   }
	   else 
	   {
		   // if they don't match 
		   myIsInserted = false;
	   }
  }
//note to self: Find a way to write all the if statements without the if statements 
  
  /**
   * Asking if key can turn NOT turning it 
   * @return
   */
  public boolean turn() 
  {
	  if (myIsInserted == true)
	  {
		  //key fits and can turn 
		  return true;
		  
	  }
	  else 
	  {
		  return false;
	  }

  }
  
  public boolean isLocked()
  {
	  if (myIsLocked == true)
	  {
		  return true;
	  }
	  else 
	  {
		  return false;
	  }
  }

  public boolean lock() 
  {
	if (turn() == true)
	{
		//will lock
		myIsLocked = true;
		return true;
	}
	else 
	{
		//will not lock
		return false;
	}
  }

  public boolean unlock() 
  {
	if (turn() == true)
	{
		//will unlock
		myIsLocked = false;
		return true;
	}
	else 
	{
		//will not unlock
		return false;
	}
  }
}
