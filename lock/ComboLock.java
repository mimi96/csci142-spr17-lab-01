package lock;

import java.util.Random;

public class ComboLock implements Lock
{
	//
	public static final int MAX_NUMBER = 50;
	// the array if integers for the combination
	private int[] myCombination;
	// an array of integers that store the attempts to open the lock
	private int[] myAttempt = new int[3];
	// whether key is locked
	private boolean myIsLocked;
	// whether combination is reset
	private boolean myIsReset;
	
	 boolean compliant = false;

	public ComboLock()
	{
		// creates a new combination / an array of integers
		Random combination = new Random();
		myCombination = new int[3];

		//
		for (int i = 0; i < 3; i++)
		{
			int combo = combination.nextInt(MAX_NUMBER);
			myCombination[i] = combo;
		}
	}

	public boolean ifComboComplient()
	{
		if ((myCombination[0] % 4) == (myCombination[2] % 4) && (myCombination[1] % 4) == ((myCombination[0] % 4) + 2) % 4)
        {
            compliant = true;
            return true;
        }
		else
		{
			return false;
		}
        
        
	}
	
	
	public void turnRight(int number)
	{
	
		// using a temporary variable to store the middle attempted number
		int temporary = myAttempt[1];

		// moving the temporary variable into the first spot
		myAttempt[0] = temporary;
		
		//move the last spot attempt into a temporary variable 
		temporary = myAttempt[2];
		
		//move new temporary value into the middle spot 
		myAttempt[1] = temporary;

		// then saving a new variable in the last spot
		myAttempt[2] = number;	
	}

	public void turnLeft(int number)
	{


		// using a temporary variable to store the middle attempted number
		int temporary = myAttempt[1];

		// moving the temporary variable into the first spot
		myAttempt[0] = temporary;
		
		//move the last spot attempt into a temporary variable 
		temporary = myAttempt[2];
		
		//move new temporary value into the middle spot 
		myAttempt[1] = temporary;

		// then saving a new variable in the last spot
		myAttempt[2] = number;
		
		
	}

	public void reset()
	{
		
	}

	public int[] getCombination()
	{
		return myCombination;
	}

	public boolean getIsReset()
	{
		return myIsReset;
	}

	public boolean isLocked()
	{
		if (myIsLocked == true)
		  {
			  return true;
		  }
		  else 
		  {
			  return false;
		  }
	}

	public boolean lock()
	{
		if (myCombination[0] == myAttempt[0])
		{
			
			if (myCombination[1] == myAttempt[1])
			{
				
				if (myCombination[2] == myAttempt[2])
				{
					
					
					myIsLocked = false;
					
					return true;
				}
			}
		}
		
		return false;
	}

	public boolean unlock()
	{
		if (myCombination[0] == myAttempt[0])
		{
			//the first positions of both match
			if (myCombination[1] == myAttempt[1])
			{
				//the second position of both match
				if (myCombination[2] == myAttempt[2])
				{
					//the last position of both match 
					
					//unlocks 
					
					myIsLocked = false;
					
					return true;
				}
			}
		}
		
		return false;
	}
}
