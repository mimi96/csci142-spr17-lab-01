package lock;

import java.util.Random;

public class KeylessEntryLock extends KeyLock
{
	public static final int MAX_NUM_USER_CODES = 10;
	public static final int USER_CODE_LENGTH = 4;
	public static final int MASTER_CODE_LENGTH = 6;
	public static final int MAX_CODE_NUMBERS = 9;

	private boolean myIsReset;
	private boolean myIsNewUserCode;
	private boolean myIsDeletedUserCode;
	private boolean myIsChangedMasterCode;
	private boolean myAllUserCodesDeleted;
	private int[] myMasterCode = new int [MASTER_CODE_LENGTH];
	private int[][] myUserCodes = new int [MAX_NUM_USER_CODES][USER_CODE_LENGTH];
	private int[] myDefaultMasterCode;
	private int[] myAttempt = new int[25];

	// created an empty string attempt to check the code in a string

	/**
	 * 
	 * KeylessEntryLock class sets up my pass codes
	 * 
	 * @param keyValue
	 */
	public KeylessEntryLock(int keyValue)
	{

		// call keyValue from keyLock
		super(keyValue);

		myMasterCode = new int[]
		{ 1, 2, 3, 1, 2, 3 };

	}

	public boolean pushKey(char key)
	{

		// using a temporary variable to store the middle attempted number
		

		// moving the temporary variable into the first spot
		

		// then saving a new variable in the last spot
		
		
		switch (key)
		{
		case '*':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				// for the character * I am saving it as an integer in order to work with the array of integers 
				myAttempt[i] = -1;
			}
			break;
		case '0':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 0;
			}
			break;
		case '1':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 1;
			}
			break;
		case '2':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 2;
			}
			break;
		case '3':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 3;
			}
			break;
		case '4':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 4;
			}
			break;
		case '5':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 5;
			}
			break;
		case '6':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 6;
			}
			break;
		case '7':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 7;
			}
			break;
		case '8':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 8;
			}
			break;
		case '9':
			for (int i = 1; i < 25; i++)
			{
				int temporary = myAttempt[i];
				myAttempt[i-1] = temporary;
				myAttempt[i] = 9;
			}
			break;
		}
		return true;
	}

	public boolean addedUserCode()
	{
		// checks if new user was added

		if (myIsNewUserCode = true)
		{
			return true;
		} else
		{
			return false;
		}
	}

	public boolean deletedUserCode()
	{
		// checks if user code is deleted
		if (myIsDeletedUserCode = true)
		{
			return true;
		} else
		{
			return false;
		}
	}
	
	private void newUserCode(int[] code)
	{
		
	}

	public boolean clearedAllUserCodes()
	{

		// checks if all user codes are deleted
		if (myAllUserCodesDeleted = true)
		{
			return true;
		} else
		{
			return false;
		}
	}

	public boolean getIsReset()
	{
		return myIsReset;
	}

	public boolean changedMasterCode()
	{

		// checks if master code is changed

		if (myIsChangedMasterCode = true)
		{
			return true;
		} else
		{
			return false;
		}
	}

	public int[] getDefaultMasterCode()
	{
		// returns the default master code
		return myDefaultMasterCode;
	}

	public boolean isLocked()
	{
		// the key is locked
		if (super.isLocked() == true)
		{
			return true;
		} else
		{
			return false;
		}

	}

	public boolean lock()
	{

		if (super.lock() == true)
		{
			// will lock
			return true;
		} else
		{
			// will not lock
			return false;
		}
	}

	public boolean unlock()
	{
//		for (int p=0; p < 6; p++)
//		{
//			if (myMasterCode[p] != myAttempt[p])
//			{
//				//if they are not equal the lock will not open 
//				return false;
//			}
//		
//		}
		
		for (int i=0; i < 9; i++)
		{
			for (int p=0; p < 4; p++)
			{
				if (myUserCodes[i][p] != myAttempt[p])
				{
					//if they are not equal the lock will not open 
					return false;
				}
			
			}
		
		}
		
		return true;
	}

}
